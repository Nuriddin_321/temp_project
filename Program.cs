﻿

using System.Diagnostics;
namespace AsyncBreakfast
{
    internal class Coffee { }
    internal class Egg { }
    internal class Juice { }
    internal class Toast { }

    class Program
    {
        static async Task Main(string[] args)
        { 

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Coffee cup = PourCoffee();
            Console.WriteLine($"Coffee is ready");

            var eggsTask = FryEggsAsync(3);
            var toastTask = MakeToastWithButterAndJamAsync(2);
            
            // await Task.WhenAll(eggsTask, toastTask);
            // Console.WriteLine("Eggs are ready"); 
            // Console.WriteLine("Toast is ready");
            // Console.WriteLine("Breakfast is ready!");

            var breakfastTask = new List<Task> {eggsTask, toastTask};
            while(breakfastTask.Count > 0)
            {
                Task finishedTask = await Task.WhenAny(breakfastTask);
                if(finishedTask == eggsTask)
                {
                    Console.WriteLine($"eggs are ready");
                    
                }
                else if(finishedTask == toastTask)
                {
                    Console.WriteLine($"toasts are ready");
                }
                breakfastTask.Remove(finishedTask);
            }
            
            Juice oj = PourOJ();
            Console.WriteLine($"juice is ready");
            Console.WriteLine($"breakfast is ready");

            Console.WriteLine($"breakfast is ready in {stopwatch.ElapsedMilliseconds:F0}ms");


        }



        private static Juice PourOJ()
        {
            Console.WriteLine($"pouring orange juice");
            return new Juice();
        }
        private static async Task<Toast> MakeToastWithButterAndJamAsync(int num)
        {
            var toast = await ToastBreadAsync(num);
            ApplyButter(toast);
            ApplyJam(toast);

            return toast;
        }
        private static void ApplyJam(Toast toast)
            => Console.WriteLine($"Putting the jam on the toast");

        private static void ApplyButter(Toast toast)
            => Console.WriteLine($"Putting butter on the toast");

        private static async Task<Toast> ToastBreadAsync(int slice)
        {
            for (int i = 0; i < slice; i++)
            {
                Console.WriteLine($"Putting a slice of the bread in the toast");
            }
            Console.WriteLine($"Start toasting...");
            await Task.Delay(3000);
            Console.WriteLine($"Remove toast from toaster ");

            return new Toast();

        }

        private static async Task<Egg> FryEggsAsync(int howMany)
        {
            Console.WriteLine($"Warming the egg pan..");
            await Task.Delay(2000);
            Console.WriteLine($"Cracking {howMany} eggs ");
            Console.WriteLine($"Cooking the eggs");
            await Task.Delay(3000);
            Console.WriteLine($"Put eggs on plate");

            return new Egg();

        }

        private static Coffee PourCoffee()
        {
            Console.WriteLine($"Pouring coffee");
            return new Coffee();
        }


    }
}